<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Comment;
use Faker\Generator as Faker;

$factory->define(Comment::class, function (Faker $faker) {
    return [
        'client_id'=>$faker->numberBetween(1,51),
        'blog_id'=>$faker->numberBetween(1,50),
        'content'=>$faker->text(200),
        'created_at'=>new DateTime,
        'updated_at'=>new DateTime,
    ];
});
