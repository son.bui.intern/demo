<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Rating;
use Faker\Generator as Faker;

$factory->define(Rating::class, function (Faker $faker) {
    return [
        'product_id'=>$faker->numberBetween(1,50),
        'client_id'=>$faker->numberBetween(1,51),
        'star_number'=>$faker->numberBetween(1,5),
        'comment'=>$faker->text(200),
        'created_at'=>new DateTime,
        'updated_at'=>new DateTime,
    ];
});
