<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Client;
use Faker\Generator as Faker;

$factory->define(Client::class, function (Faker $faker) {
    $password = bcrypt('1');
    return [
        'name'=>$faker->name,
        'email'=>$faker->unique()->safeEmail,
        'avatar'=>'http://localhost/lrv_project/public/home/avatars/avatar1.jpg',
        'phone'=>$faker->phoneNumber,
        'password'=>$password,
        'birthday'=>$faker->date('Y-m-d','now'),
        'gender'=>$faker->numberBetween(0,1),
        'created_at'=>new DateTime,
        'updated_at'=>new DateTime,
    ];
});
