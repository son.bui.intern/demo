<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Favorite;
use Faker\Generator as Faker;

$factory->define(Favorite::class, function (Faker $faker) {
    return [
        'product_id'=>$faker->numberBetween(1,50),
        'client_id'=>$faker->numberBetween(1,51),
        'created_at'=>new DateTime,
        'updated_at'=>new DateTime,
    ];
});
