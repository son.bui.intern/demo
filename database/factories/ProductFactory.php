<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    $name = $faker->name;
    $slug = \Str::slug($name);
    $price = $faker->numberBetween(0,500);
    $sale_price = $faker->numberBetween(0,$price);
    $text = $faker->text;
    return [
        'name'=>$name,
        'slug'=>$slug,
        'image'=>'http://localhost/lrv_project/public/image/products/01_04_Home_14.png',
        'price'=>$price,
        'sale_price'=>$sale_price,
        'sumary'=>$text,
        'content'=>$text,
        'meta_key'=>$name,
        'meta_title'=>$text,
        'meta_desc'=>$text,
        'category_id'=>$faker->numberBetween(1,4),
        'brand_id'=>$faker->numberBetween(1,4),
        'created_at'=>new DateTime,
        'updated_at'=>new DateTime,
    ];
});
