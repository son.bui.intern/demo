<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Subcriber;
use Faker\Generator as Faker;

$factory->define(Subcriber::class, function (Faker $faker) {
    return [
        'email'=>$faker->email,
        'status'=>$faker->numberBetween(0,1),
        'created_at'=>new DateTime,
        'updated_at'=>new DateTime,
    ];
});
