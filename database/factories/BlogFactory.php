<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Blog;
use Faker\Generator as Faker;

$factory->define(Blog::class, function (Faker $faker) {
    $name = \Str::random(5).'Lorem ipsum dolor sit amet consectetur adipisicing elit. Recusandae cum provident, fuga.';
    $slug = \Str::slug($name);
    $title = Str::random(20);
    $common = $faker->text;
    return [
        'category_id'=>$faker->numberBetween(1,4),
        'name'=>$name,
        'slug'=>$slug,
        'image'=>'http://localhost/lrv_project/public/image/blog/01_04_Home_02.png',
        'content'=>'<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

    <p><strong>Wash your face twice</strong> - Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
    tempor incididunt ut labore et dolore magna aliqua.</p>
    <p><strong>Eat healthy</strong> - Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non
    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
    <img src="image/images/01_04_Home_18.png" alt="" width="100%">
    <p><strong>Drink loads of water</strong> - Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
    <p><strong>Have a skin regime</strong> - Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non
    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

    <div  class="quote">
        <h4 data-aos="flip-right" data-aos-duration="1500"><strong>Avoid having an erratic sleep pattern and do not let your work create a lot of stress for you either.</strong></h4>
        <span ><i class="fa fa-quote-left"></i></span><span class="author"> Trevor Lawson</span>
    </div>

    <p><strong>Sleepping beauty</strong> - Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>',
        'meta_title'=>$title,
        'meta_desc'=>$common,
        'created_at'=>new DateTime,
        'updated_at'=>new DateTime,
    ];
});