<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Order;
use Faker\Generator as Faker;

$factory->define(Order::class, function (Faker $faker) {
    return [
        'client_id'=>$faker->numberBetween(1,51),
        'total_amount'=>$faker->numberBetween(200,2000),
        'name'=>$faker->name,
        'address'=>$faker->address,
        'phone'=>$faker->phoneNumber,
        'payment_id'=>$faker->numberBetween(1,2),
        'shipping_id'=>$faker->numberBetween(1,3),
        'created_at'=>new DateTime,
        'updated_at'=>new DateTime,
    ];
});
