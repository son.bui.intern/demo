<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Quote;
use Faker\Generator as Faker;

$factory->define(Quote::class, function (Faker $faker) {
    return [
        'name'=>$faker->name,
        'address'=>$faker->address,
        'message'=>$faker->text(100),
        'created_at'=>new DateTime,
        'updated_at'=>new DateTime,
    ];
});
