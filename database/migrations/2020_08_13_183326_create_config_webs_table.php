<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConfigWebsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('config_webs', function (Blueprint $table) {
            $table->id();
            $table->string('logo_top')->nullable();
            $table->string('logo_bot')->nullable();
            $table->string('email')->nullable();
            $table->string('hotline')->nullable();
            $table->string('address_contact')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('config_webs');
    }
}
