<?php

use Illuminate\Database\Seeder;

class PaymentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('payments')->insert([
            ['name'=>'PayPal','created_at'=>new DateTime,'updated_at'=>new DateTime],
            ['name'=>'FE Credit','created_at'=>new DateTime,'updated_at'=>new DateTime],
        ]);
    }
}
