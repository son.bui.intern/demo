<?php

use Illuminate\Database\Seeder;
use Faker\Generator as Faker;

class BrandSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        DB::table('brands')->insert([
            ['name'=>'Kosas','slug'=>'kosas','image'=>$faker->imageUrl(),'created_at'=>new DateTime,'updated_at'=>new DateTime],
            ['name'=>'The Retro','slug'=>'the-retro','image'=>$faker->imageUrl(),'created_at'=>new DateTime,'updated_at'=>new DateTime],
            ['name'=>'Hipster','slug'=>'hipster','image'=>$faker->imageUrl(),'created_at'=>new DateTime,'updated_at'=>new DateTime],
            ['name'=>'Iconic','slug'=>'Iconic','image'=>$faker->imageUrl(),'created_at'=>new DateTime,'updated_at'=>new DateTime],
            ['name'=>'The BackYard','slug'=>'the-backyard','image'=>$faker->imageUrl(),'created_at'=>new DateTime,'updated_at'=>new DateTime],
        ]);
    }
}
