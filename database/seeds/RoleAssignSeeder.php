<?php

use Illuminate\Database\Seeder;
use Faker\Generator as Faker;
use App\User;

class RoleAssignSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        $users = User::all();
        DB::table('user_roles')->insert([
            [
                'user_id'=>'1',
                'role_id'=>'1',
            ]
        ]);
        foreach($users as $user){
            if($user->id!=1){
                DB::table('user_roles')->insert([
                    [
                        'user_id'=>$user->id,
                        'role_id'=>'2',
                    ],
                    [
                        'user_id'=>$user->id,
                        'role_id'=>$faker->numberBetween(3,8),
                    ],
                ]);
            }
        }
    }
}
