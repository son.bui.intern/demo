<?php

use Illuminate\Database\Seeder;
use Faker\Generator as Faker;

class BannerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        DB::table('banners')->insert([
            ['name'=>$faker->name,'description'=>$faker->text(50),'image'=>'http://localhost/lrv_project/public/image/banners/01_04_Home_02.png','link'=>$faker->text(20),'position'=>'top','created_at'=>new DateTime,'updated_at'=>new DateTime],
            ['name'=>$faker->name,'description'=>$faker->text(50),'image'=>'http://localhost/lrv_project/public/image/banners/01_04_Home_18.png','link'=>$faker->text(20),'position'=>'top','created_at'=>new DateTime,'updated_at'=>new DateTime],
            ['name'=>$faker->name,'description'=>$faker->text(50),'image'=>'http://localhost/lrv_project/public/image/banners/01_04_Home_02.png','link'=>$faker->text(20),'position'=>'top','created_at'=>new DateTime,'updated_at'=>new DateTime],
            ['name'=>$faker->name,'description'=>$faker->text(50),'image'=>'http://localhost/lrv_project/public/image/banners/01_04_Home_02.png','link'=>$faker->text(20),'position'=>'top','created_at'=>new DateTime,'updated_at'=>new DateTime],
            ['name'=>$faker->name,'description'=>$faker->text(50),'image'=>'http://localhost/lrv_project/public/image/banners/01_04_Home_14.png','link'=>$faker->text(20),'position'=>'mid','created_at'=>new DateTime,'updated_at'=>new DateTime],
            ['name'=>$faker->name,'description'=>$faker->text(50),'image'=>'http://localhost/lrv_project/public/image/banners/01_04_Home_14.png','link'=>$faker->text(20),'position'=>'mid','created_at'=>new DateTime,'updated_at'=>new DateTime],
            ['name'=>$faker->name,'description'=>$faker->text(50),'image'=>'http://localhost/lrv_project/public/image/banners/01_04_Home_30_01.png','link'=>$faker->text(20),'position'=>'bot','created_at'=>new DateTime,'updated_at'=>new DateTime],
            ['name'=>$faker->name,'description'=>$faker->text(50),'image'=>'http://localhost/lrv_project/public/image/banners/01_04_Home_30_02.png','link'=>$faker->text(20),'position'=>'bot','created_at'=>new DateTime,'updated_at'=>new DateTime],
            ['name'=>$faker->name,'description'=>$faker->text(50),'image'=>'http://localhost/lrv_project/public/image/banners/01_04_Home_30_03.png','link'=>$faker->text(20),'position'=>'bot','created_at'=>new DateTime,'updated_at'=>new DateTime],
            ['name'=>$faker->name,'description'=>$faker->text(50),'image'=>'http://localhost/lrv_project/public/image/banners/01_04_Home_30_04.png','link'=>$faker->text(20),'position'=>'bot','created_at'=>new DateTime,'updated_at'=>new DateTime],
            ['name'=>$faker->name,'description'=>$faker->text(50),'image'=>'http://localhost/lrv_project/public/image/banners/01_04_Home_30_05.png','link'=>$faker->text(20),'position'=>'bot','created_at'=>new DateTime,'updated_at'=>new DateTime],
            ['name'=>$faker->name,'description'=>$faker->text(50),'image'=>'http://localhost/lrv_project/public/image/banners/01_04_Home_30_06.png','link'=>$faker->text(20),'position'=>'bot','created_at'=>new DateTime,'updated_at'=>new DateTime],
        ]);
    }
}
