<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CategorySeeder::class);
        $this->call(BrandSeeder::class);
        $this->call(ColorSeeder::class);
        $this->call(ProductSeeder::class);
        $this->call(BlogSeeder::class);
        $this->call(ClientSeeder::class);
        $this->call(CommentSeeder::class);
        $this->call(FavoriteSeeder::class);
        $this->call(ProductRatingSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(RoleSeeder::class);
        $this->call(RoleAssignSeeder::class);
        $this->call(PaymentSeeder::class);
        $this->call(ShippingSeeder::class);
        $this->call(ProductDetailSeeder::class);
        $this->call(ProductImageSeeder::class);
        $this->call(OrderSeeder::class);
        $this->call(QuoteSeeder::class);
        $this->call(SubcriberSeeder::class);
        $this->call(ConfigWebSeeder::class);
        $this->call(BannerSeeder::class);
    }
}
