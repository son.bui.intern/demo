<?php

use Illuminate\Database\Seeder;
use App\Models\Client;

class ClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('clients')->insert([
            [
                'name'=>'Bùi Thiện Sơn',
                'email'=>'sonbui234@gmail.com',
                'phone'=>'0147852369',
                'password'=>bcrypt('1'),
                'birthday'=>'1995-04-24',
                'gender'=>'1',
                'created_at'=>new DateTime,
                'updated_at'=>new DateTime
            ]
        ]);
        factory(Client::class,50)->create();
    }
}