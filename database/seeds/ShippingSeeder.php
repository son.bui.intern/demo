<?php

use Illuminate\Database\Seeder;

class ShippingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('shippings')->insert([
            ['name'=>'Air Transport','created_at'=>new DateTime,'updated_at'=>new DateTime],
            ['name'=>'Sea Transport','created_at'=>new DateTime,'updated_at'=>new DateTime],
            ['name'=>'Road Transport','created_at'=>new DateTime,'updated_at'=>new DateTime],
        ]);
    }
}
