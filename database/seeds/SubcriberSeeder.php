<?php

use Illuminate\Database\Seeder;
use App\Models\Subcriber;

class SubcriberSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Subcriber::class,50)->create();
    }
}
