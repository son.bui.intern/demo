<?php

use Illuminate\Database\Seeder;
use App\Models\Order;
use Faker\Generator as Faker;

class OrderDetailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        $orders = Order::all();
        foreach($orders as $order){
            DB::table('order_details')->insert([
                ['order_id'=>$order->id,'product_detail_id'=>$faker->numberBetween(1,51),'quantity'=>$faker->numberBetween(1,10),'price'=>$faker->numberBetween(1,500),'created_at'=>new DateTime,'updated_at'=>new DateTime],
                ['order_id'=>$order->id,'product_detail_id'=>$faker->numberBetween(1,51),'quantity'=>$faker->numberBetween(1,10),'price'=>$faker->numberBetween(1,500),'created_at'=>new DateTime,'updated_at'=>new DateTime],
                ['order_id'=>$order->id,'product_detail_id'=>$faker->numberBetween(1,51),'quantity'=>$faker->numberBetween(1,10),'price'=>$faker->numberBetween(1,500),'created_at'=>new DateTime,'updated_at'=>new DateTime],
            ]);
        }
    }
}
