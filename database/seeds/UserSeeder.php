<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name'=>'Admin',
                'email'=>'admin@gmail.com',
                'phone'=>'0147852369',
                'password'=>bcrypt('1'),
                'birtday'=>'1995-04-24',
                'gender'=>'1',
                'created_at'=>new DateTime,
                'updated_at'=>new DateTime
            ]
        ]);
        factory(User::class,5)->create();
    }
}
