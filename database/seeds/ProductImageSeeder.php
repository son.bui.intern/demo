<?php

use Illuminate\Database\Seeder;
use Faker\Generator as Faker;
use App\Models\Product;

class ProductImageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        $products = Product::all();
        foreach($products as $pro){
            DB::table('product_images')->insert([
                ['product_id'=>$pro->id,'image'=>'http://localhost/lrv_project/public/image/products/01_04_Home_5.png','created_at'=>new DateTime,'updated_at'=>new DateTime],
                ['product_id'=>$pro->id,'image'=>'http://localhost/lrv_project/public/image/products/01_04_Home_7.png','created_at'=>new DateTime,'updated_at'=>new DateTime],
                ['product_id'=>$pro->id,'image'=>'http://localhost/lrv_project/public/image/products/01_04_Home_9.png','created_at'=>new DateTime,'updated_at'=>new DateTime],
                ['product_id'=>$pro->id,'image'=>'http://localhost/lrv_project/public/image/products/01_04_Home_13.png','created_at'=>new DateTime,'updated_at'=>new DateTime],
                ['product_id'=>$pro->id,'image'=>'http://localhost/lrv_project/public/image/products/01_04_Home_14.png','created_at'=>new DateTime,'updated_at'=>new DateTime],
            ]);
        }
    }
}
