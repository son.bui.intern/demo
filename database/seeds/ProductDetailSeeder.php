<?php

use Illuminate\Database\Seeder;
use Faker\Generator as Faker;
use App\Models\Product;

class ProductDetailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        $products = Product::all();
        foreach($products as $pro){
            DB::table('product_details')->insert([
                ['product_id'=>$pro->id,'color_id'=>$faker->numberBetween(1,4),'created_at'=>new DateTime,'updated_at'=>new DateTime],
            ]);
        }
    }
}
