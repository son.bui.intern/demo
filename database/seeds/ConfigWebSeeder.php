<?php

use Illuminate\Database\Seeder;
use Faker\Generator as Faker;

class ConfigWebSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        DB::table('config_webs')->insert([
            [
                'logo_top'=>'http://localhost/lrv_project/public/image/logo/logo2.png',
                'logo_bot'=>'http://localhost/lrv_project/public/image/logo/logo.png',
                'email'=>$faker->email,
                'hotline'=>$faker->phoneNumber,
                'address_contact'=>$faker->address,
                'created_at'=>new DateTime,
                'updated_at'=>new DateTime
            ],
            
        ]);
    }
}
