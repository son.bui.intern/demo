<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            ['name'=>'Beauty','slug'=>'beauty','created_at'=>new DateTime,'updated_at'=>new DateTime],
            ['name'=>'Make up','slug'=>'make-up','created_at'=>new DateTime,'updated_at'=>new DateTime],
            ['name'=>'Skin care','slug'=>'skin-care','created_at'=>new DateTime,'updated_at'=>new DateTime],
            ['name'=>'Body care','slug'=>'body-care','created_at'=>new DateTime,'updated_at'=>new DateTime],
        ]);
    }
}
