<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            [
                'name'=>'Admin',
                'permissions'=>'["admin.login","admin.post_login","admin.register","admin.logout","admin.post_register","admin.error","admin.index","admin.config.index","admin.config.create","admin.config.store","admin.config.edit","admin.config.update","admin.info","admin.profile","admin.avatar","admin.Category.index","admin.Category.create","admin.Category.store","admin.Category.show","admin.Category.edit","admin.Category.update","admin.Category.destroy","admin.Product.index","admin.Product.create","admin.Product.store","admin.Product.show","admin.Product.edit","admin.Product.update","admin.Product.destroy","admin.Brand.index","admin.Brand.create","admin.Brand.store","admin.Brand.show","admin.Brand.edit","admin.Brand.update","admin.Brand.destroy","admin.Color.index","admin.Color.create","admin.Color.store","admin.Color.show","admin.Color.edit","admin.Color.update","admin.Color.destroy","admin.Role.index","admin.Role.create","admin.Role.store","admin.Role.show","admin.Role.edit","admin.Role.update","admin.Role.destroy","admin.User.index","admin.User.create","admin.User.store","admin.User.show","admin.User.edit","admin.User.update","admin.User.destroy","admin.Blog.index","admin.Blog.create","admin.Blog.store","admin.Blog.show","admin.Blog.edit","admin.Blog.update","admin.Blog.destroy","admin.Comment.index","admin.Comment.create","admin.Comment.store","admin.Comment.show","admin.Comment.edit","admin.Comment.update","admin.Comment.destroy","admin.Payment.index","admin.Payment.create","admin.Payment.store","admin.Payment.show","admin.Payment.edit","admin.Payment.update","admin.Payment.destroy","admin.Shipping.index","admin.Shipping.create","admin.Shipping.store","admin.Shipping.show","admin.Shipping.edit","admin.Shipping.update","admin.Shipping.destroy","admin.Client.index","admin.Client.create","admin.Client.store","admin.Client.show","admin.Client.edit","admin.Client.update","admin.Client.destroy","admin.Banner.index","admin.Banner.create","admin.Banner.store","admin.Banner.show","admin.Banner.edit","admin.Banner.update","admin.Banner.destroy","admin.Subcriber.index","admin.Subcriber.create","admin.Subcriber.store","admin.Subcriber.show","admin.Subcriber.edit","admin.Subcriber.update","admin.Subcriber.destroy","admin.Contact.index","admin.Contact.create","admin.Contact.store","admin.Contact.show","admin.Contact.edit","admin.Contact.update","admin.Contact.destroy","admin.Quote.index","admin.Quote.create","admin.Quote.store","admin.Quote.show","admin.Quote.edit","admin.Quote.update","admin.Quote.destroy","admin.Order.index","admin.Order.create","admin.Order.store","admin.Order.show","admin.Order.edit","admin.Order.update","admin.Order.destroy"]',
                'created_at'=>new DateTime,
                'updated_at'=>new DateTime
            ],
            [
                'name'=>'New',
                'permissions'=>'["admin.logout","admin.index","admin.info","admin.profile","admin.avatar","admin.change_pass","admin.post_password"]',
                'created_at'=>new DateTime,
                'updated_at'=>new DateTime
            ],
            [
                'name'=>'Product',
                'permissions'=>'["admin.Category.index","admin.Category.create","admin.Category.store","admin.Category.show","admin.Category.edit","admin.Category.update","admin.Category.destroy","admin.Product.index","admin.Product.create","admin.Product.store","admin.Product.show","admin.Product.edit","admin.Product.update","admin.Product.destroy","admin.Brand.index","admin.Brand.create","admin.Brand.store","admin.Brand.show","admin.Brand.edit","admin.Brand.update","admin.Brand.destroy","admin.Color.index","admin.Color.create","admin.Color.store","admin.Color.show","admin.Color.edit","admin.Color.update","admin.Color.destroy"]',
                'created_at'=>new DateTime,
                'updated_at'=>new DateTime
            ],
            [
                'name'=>'Blog',
                'permissions'=>'["admin.Blog.index","admin.Blog.create","admin.Blog.store","admin.Blog.show","admin.Blog.edit","admin.Blog.update","admin.Blog.destroy","admin.Comment.index","admin.Comment.create","admin.Comment.store","admin.Comment.show","admin.Comment.edit","admin.Comment.update","admin.Comment.destroy"]',
                'created_at'=>new DateTime,
                'updated_at'=>new DateTime
            ],
            [
                'name'=>'Customer',
                'permissions'=>'["admin.Client.index","admin.Client.create","admin.Client.store","admin.Client.show","admin.Client.edit","admin.Client.update","admin.Client.destroy"]',
                'created_at'=>new DateTime,
                'updated_at'=>new DateTime
            ],
            [
                'name'=>'Order',
                'permissions'=>'["admin.Order.index","admin.Order.create","admin.Order.store","admin.Order.show","admin.Order.edit","admin.Order.update","admin.Order.destroy"]',
                'created_at'=>new DateTime,
                'updated_at'=>new DateTime
            ],
            [
                'name'=>'Review',
                'permissions'=>'["admin.Comment.index","admin.Comment.create","admin.Comment.store","admin.Comment.show","admin.Comment.edit","admin.Comment.update","admin.Comment.destroy","admin.Subcriber.index","admin.Subcriber.create","admin.Subcriber.store","admin.Subcriber.show","admin.Subcriber.edit","admin.Subcriber.update","admin.Subcriber.destroy","admin.Contact.index","admin.Contact.create","admin.Contact.store","admin.Contact.show","admin.Contact.edit","admin.Contact.update","admin.Contact.destroy","admin.Quote.index","admin.Quote.create","admin.Quote.store","admin.Quote.show","admin.Quote.edit","admin.Quote.update","admin.Quote.destroy"]',
                'created_at'=>new DateTime,
                'updated_at'=>new DateTime
            ],
            [
                'name'=>'Config',
                'permissions'=>'["admin.config.index","admin.config.create","admin.config.store","admin.config.edit","admin.config.update","admin.Payment.index","admin.Payment.create","admin.Payment.store","admin.Payment.show","admin.Payment.edit","admin.Payment.update","admin.Payment.destroy","admin.Shipping.index","admin.Shipping.create","admin.Shipping.store","admin.Shipping.show","admin.Shipping.edit","admin.Shipping.update","admin.Shipping.destroy","admin.Banner.index","admin.Banner.create","admin.Banner.store","admin.Banner.show","admin.Banner.edit","admin.Banner.update","admin.Banner.destroy"]',
                'created_at'=>new DateTime,
                'updated_at'=>new DateTime
            ],
        ]);
    }
}
