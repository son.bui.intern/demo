<?php

use Illuminate\Database\Seeder;

class ColorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('colors')->insert([
            ['name'=>'Red','status'=>'1','priority'=>'1','created_at'=>new DateTime,'updated_at'=>new DateTime],
            ['name'=>'Blue','status'=>'1','priority'=>'1','created_at'=>new DateTime,'updated_at'=>new DateTime],
            ['name'=>'Black','status'=>'1','priority'=>'1','created_at'=>new DateTime,'updated_at'=>new DateTime],
            ['name'=>'Green','status'=>'1','priority'=>'1','created_at'=>new DateTime,'updated_at'=>new DateTime],
        ]);
    }
}
