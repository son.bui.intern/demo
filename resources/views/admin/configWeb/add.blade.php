@extends('layout.admin')
@section('title','CONFIG')
@section('bar_form')
<a href="{{route('admin.config.index')}}" style="margin:10px" title="Về danh sách" class="btn btn-info btn-sm ml-1"><i
        class="fa fa-undo"></i> Back</a>
@stop()
@section('main')

<div class="panel panel-primary">
    <div class="panel-heading">
        <h3>Add New</h3>
    </div>
    <div class="panel-body">
        <form action="{{route('admin.config.store')}}" method="POST" role="form">
            @csrf
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Logo Top</label>
                    <div class="form-group">
                        <img src="" id="show_top" width="100%" alt="">
                    </div>
                    <div class="input-group">
                        <input type="text" class="form-control" id="show_img_top" name="logo_top">
                        <span class="input-group-btn">
                            <a class="btn btn-primary" data-toggle="modal" href='#modal-id-top'>Select</a>
                        </span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="">Email</label>
                    <input type="text" class="form-control" name="email">
                </div>
                <div class="form-group">
                    <label for="">Hot line</label>
                    <input type="text" class="form-control" name="hotline">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Logo Bot</label>
                    <div class="form-group">
                        <img src="" id="show_bot" width="100%" alt="">
                    </div>
                    <div class="input-group">
                        <input type="text" class="form-control" id="show_img_bot" name="logo_bot">
                        <span class="input-group-btn">
                            <a class="btn btn-primary" data-toggle="modal" href='#modal-id-bot'>Select</a>
                        </span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="">Address Contact</label>
                    <input type="text" class="form-control" name="address_contact">
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="text-center">
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
        </form>
    </div>
</div>
<div class="modal fade" id="modal-id-top">
    <div class="modal-dialog" style="width:90%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Ảnh</h4>
            </div>
            <div class="modal-body">
                <iframe
                    src="{{url('public/filemanager')}}/dialog.php?akey=JDmgWEZBvxs9fYw6jRK7qsB5QJnafF4ig39kw4ygs&field_id=show_img_top"
                    style="height:500px; width:100%" frameborder="0"></iframe>
            </div>

        </div>
    </div>
</div>
<div class="modal fade" id="modal-id-bot">
    <div class="modal-dialog" style="width:90%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Ảnh</h4>
            </div>
            <div class="modal-body">
                <iframe
                    src="{{url('public/filemanager')}}/dialog.php?akey=JDmgWEZBvxs9fYw6jRK7qsB5QJnafF4ig39kw4ygs&field_id=show_img_bot"
                    style="height:500px; width:100%" frameborder="0"></iframe>
            </div>

        </div>
    </div>
</div>

@stop()

@section('js')

<script>
$('#modal-id-top').on('hide.bs.modal', function() {
    var show = $('#show_img_top').val();
    $('#show_top').attr('src', show);
});
$('#modal-id-bot').on('hide.bs.modal', function() {
    var show = $('#show_img_bot').val();
    $('#show_bot').attr('src', show);
})
</script>
@if(Session::has('success'))
<script type="text/javascript">
toastr.success("{{Session::get('success')}}")
</script>
@endif

@if(Session::has('error'))
<script type="text/javascript">
toastr.error("{{Session::get('error')}}")
</script>
@endif

@stop()