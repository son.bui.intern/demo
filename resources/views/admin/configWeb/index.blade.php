@extends('layout.admin')
@section('title','CONFIG')
@section('bar_form')
<a href="{{route('admin.index')}}" style="margin:10px" title="Về danh sách" class="btn btn-info btn-sm ml-1"><i
        class="fa fa-undo"></i> Back</a>
@stop()
@section('main')

<div class="panel panel-primary">
    <div class="panel-heading">
        @if(empty($config))
        <a class="btn btn-success" href="{{route('admin.config.create')}}">Add New</a>
        @endif
    </div>
    <div class="panel-body">
    @if(!empty($config))
        <div class="col-md-4">
            <div class="form-group">
                <label for="">Logo Top</label>
                <img src="{{$config->logo_top}}" width="100%" style="background: #000;" alt="">
                <a href=""></a>
            </div>
            <div class="form-group">
                <label for="">Logo Bot</label>
                <img src="{{$config->logo_bot}}" width="100%" alt="">
            </div>
            <div class="text-center">
                <a href="{{route('admin.config.edit',$config->id)}}" class="btn btn-primary">Change</a>
            </div>

        </div>
        <div class="col-md-8">
            <div class="form-group">
                <label for="">Email</label>
                <form action="{{route('admin.config.update',$config->id)}}" method="POST" role="form">
                    @csrf @method('PUT')
                    <div class="input-group">
                        <input type="text" class="form-control" name="email" value="{{$config->email}}">
                        <span class="input-group-btn">
                            <button type="submit" class="btn btn-primary">Change</button>
                        </span>
                    </div>
                </form>
            </div>
            <div class="form-group">
                <label for="">Hotline</label>
                <form action="{{route('admin.config.update',$config->id)}}" method="POST" role="form">
                    @csrf @method('PUT')
                    <div class="input-group">
                        <input type="text" class="form-control" name="hotline" value="{{$config->hotline}}">
                        <span class="input-group-btn">
                            <button type="submit" class="btn btn-primary">Change</button>
                        </span>
                    </div>
                </form>
            </div>
            <div class="form-group">
                <label for="">Address Contact</label>
                <form action="{{route('admin.config.update',$config->id)}}" method="POST" role="form">
                    @csrf @method('PUT')
                    <div class="input-group">
                        <input type="text" class="form-control" name="address_contact"
                            value="{{$config->address_contact}}">
                        <span class="input-group-btn">
                            <button type="submit" class="btn btn-primary">Change</button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @endif
</div>


@stop()

@section('js')

@if(Session::has('success'))
<script type="text/javascript">
toastr.success("{{Session::get('success')}}")
</script>
@endif

@if(Session::has('error'))
<script type="text/javascript">
toastr.error("{{Session::get('error')}}")
</script>
@endif

@stop()