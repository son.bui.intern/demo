@extends('layout.admin')

@section('css')
<link rel="stylesheet" href="{{url('public/Admin/dist/css/admin')}}/profile.css">
@stop()

@section('bar_form')
<a href="{{route('admin.index')}}" style="margin:10px" title="Về danh sách" class="btn btn-info btn-sm ml-1"><i
        class="fa fa-undo"></i> Back</a>
@stop()

@section('main')
@if(Auth::user())
<div class="container">
    <legend>ADMIN : {{Auth::user()->name}}</legend>
    <div class="container">
        <div class="col-xs-12 col-md-6 col-md-offset-3">

            <div class="panel panel-default">
                <div class="panel-heading text-center">
                    <h3 class="panel-title">Change Password</h3>
                </div>
                <div class="panel-body">
                    <form action="{{route('admin.post_password',Auth::user()->id)}}" method="POST">
                        @csrf @method('PUT')
                        <div class="form-group text-center">
                            @if(Auth::user()->avatar)
                            <img class="profile_avatar" src="{{Auth::user()->avatar}}">
                            @else
                            <img class="profile_avatar" src="{{url('public/home/avatars/default.jpg')}}">
                            @endif
                        </div>
                        
                        <div class="form-group">
                            <label for="">Old Password</label>
                            <input type="password" class="form-control" name="old_password" placeholder="Input field">
                        </div>
                        @error('old_password')
                            
                            <div class="alert alert-warning">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <strong>Warning!</strong> {{$message}}
                            </div>
                            
                        @enderror
                        <div class="form-group">
                            <label for="">New Password</label>
                            <input type="password" class="form-control" name="new_password" placeholder="Input field">
                        </div>
                        @error('new_password')
                            
                            <div class="alert alert-warning">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <strong>Warning!</strong> {{$message}}
                            </div>
                            
                        @enderror
                        <div class="form-group">
                            <label for="">Confirm New Password</label>
                            <input type="password" class="form-control" name="confirm_password" placeholder="Input field">
                        </div>
                        @error('confirm_password')
                            
                            <div class="alert alert-warning">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <strong>Warning!</strong> {{$message}}
                            </div>
                            
                        @enderror
                        <div class="text-center">
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                        
                    </form>
                </div>
            </div>

        </div>
    </div>

</div>
@endif

@stop()

@section('js')

@if(Session::has('success'))
<script type="text/javascript">
toastr.success("{{Session::get('success')}}")
</script>
@endif

@if(Session::has('error'))
<script type="text/javascript">
toastr.error("{{Session::get('error')}}")
</script>
@endif

@stop()