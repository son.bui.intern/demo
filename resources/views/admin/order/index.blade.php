@extends('layout.admin')
@section('link','admin.Order.index')
@section('title','ORDER')
@section('bar_form')
<a href="{{route('admin.index')}}" style="margin:10px" title="Về danh sách" class="btn btn-info btn-sm ml-1"><i
        class="fa fa-undo"></i> Back</a>
@stop()
@section('main')

<div class="panel panel-default">
    <div class="panel-heading">
        <form action="{{route('admin.Order.index')}}" method="GET" class="form-inline" role="form">
            <div class="form-group">
                <input class="form-control" name="key" value="{{request()->key}}">
            </div>
            <button type="submit" class="btn btn-primary">Find</button>
        </form>
    </div>
    <div class="panel-body">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Customer</th>
                    <th>Recipient</th>
                    <th>Address</th>
                    <th>Phone</th>
                    <th>Status</th>
                    <th>Total price</th>
                    <th>Created at</th>
                </tr>
            </thead>
            <tbody>
                @foreach($orders as $order)
                <tr>
                    <td>{{$order->id}}</td>
                    <td>{{$order->user_order->name}}</td>
                    <td>{{$order->name}}</td>
                    <td>{{$order->address}}</td>
                    <td>{{$order->phone}}</td>
                    <td>{{$order->status==1?'Completed':'Delivering'}}</td>
                    <td>{{$order->total_amount}}</td>
                    <td>{{$order->created_at}}</td>
                    <td>
                        <form action="{{route('admin.Order.destroy',$order->id)}}" method="POST" role="form">
                            @csrf @method('DELETE')
                            <a href="{{route('admin.Order.show',$order->id)}}" class="btn btn-primary btn-xs">Show</a>
                            <button type="submit" class="btn btn-danger btn-xs">Delete</button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {{ $orders->appends(request()->only('key'))->links() }}
    </div>
</div>

@stop()

@section('js')

@if(Session::has('success'))
<script type="text/javascript">
    toastr.success("{{Session::get('success')}}")
</script>
@endif

@if(Session::has('error'))
<script type="text/javascript">
    toastr.error("{{Session::get('error')}}")
</script>
@endif

@stop()