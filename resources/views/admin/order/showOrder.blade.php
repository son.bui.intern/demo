@extends('layout.admin')
@section('title','Order detail')
@section('bar_form')
<a href="{{route('admin.Order.index')}}" style="margin:10px" title="Về danh sách" class="btn btn-info btn-sm ml-1"><i
        class="fa fa-undo"></i> Back</a>
@stop()
@section('main')


<div class="panel panel-default">
    <div class="panel-body">
        <div class="col-xs-6">
            <div class="row">
                <h4>Customer: {{$order->user_order->name}}</h4>
                <h4>Address: {{$order->address}}</h4>
                <h4>Email: {{$order->user_order->email}}</h4>
                <h4>Order Code: {{$order->id}} </h4>
            </div>
        </div>
        <div class="col-xs-6">
            <div class="row">
                <h4>Recipient: {{$order->name}}</h4>
                <h4>Created Day: {{date('h:i:s A d,M,Y',strtotime($order->created_at))}} </h4>
                <h4>Phone: {{$order->phone}} </h4>
                <h4>Status: {{$order->status==1?'Completed':'Delivering'}} </h4>
            </div>
        </div>

        <div class="clearfix"></div>
        <h4>Order Details:</h4>
        <table border="1" cellspacing="0" cellpadding="10" width="100%">
            <thead>
                <tr style="background-color:red;color:#fff">
                    <th class="text-center" style="padding:10px">Stt</th>
                    <th class="text-center" style="padding:10px">Product</th>
                    <th class="text-center" style="padding:10px">Color</th>
                    <th class="text-center" style="padding:10px">Price</th>
                    <th class="text-center" style="padding:10px">Quantity</th>
                    <th class="text-center" style="padding:10px">Total</th>
                </tr>
            </thead>
            <tbody class="text-center">
            <?php $i = 1?>
                @foreach($order->order_detail as $or)
                <tr>
                    <td>{{$i}}</td>
                    <td>
                        <p><strong>{{$or->getproductId->getproduct->name}}</strong></p>
                    </td>
                    <td>{{$or->getproductId->color_id}}</td>
                    <td>$ {{$or->price}}</td>
                    <td>{{$or->quantity}}</td>
                    <td>$ {{$or->price*$or->quantity}}</td>
                </tr>
                <?php $i++?>
                @endforeach
            </tbody>
        </table>
        <div class="col-sm-12 col-md-6">
            <h4>Payment: {{$order->payment->name}}</h4>
            <h4>Shipping: {{$order->shipping->name}}</h4>
        </div>
        <div class="col-sm-12 col-md-6 text-right">
            <h4><strong>Total: {{$order->total_amount}} $</strong></h4>
        </div>
    </div>
</div>


@stop

@section('js')

<script>
$('#modal-id').on('hide.bs.modal', function() {
    var show = $('#show_img').val();
    $('#show').attr('src', show);
})
</script>

@stop