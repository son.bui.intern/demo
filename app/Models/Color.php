<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Color extends Model
{
    protected $fillable = ['name', 'status','priority'];

    public function product_detail()
    {
        return $this->hasMany(Product_detail::class,'color_id','id');
    }

    public function scopeSearch($query){
        if(request()->key){
            $keyword = request()->key;
            $query->where('name','LIKE','%'.$keyword.'%');
        }
        return $query;
    }

    public function scopeAdd(){
        $model = $this->create([
            'name'=>request()->name,
            'status'=>request()->status,
            'priority'=>request()->priority,
        ]);
        if($model){
            return $noti = ['type'=>'success','message'=>'Successfully added!'];
         }else{
            return $noti = ['type'=>'error','message'=>'New addition failed!'];
         }
    }
    public function scopeModify($query,$id){
        $color = $this->find($id);
        $model = $color->update(request()->except('_method','_token','id'));
        if($model){
            return $noti = ['type'=>'success','message'=>'Successfully fixed!'];
        }else{
            return $noti = ['type'=>'error','message'=>'Fix failed!'];
        }
    }
    public function scopeRemove($query,$id){
        $color = $this->find($id);
        $count = $color->product_detail()->count();
        if($count > 0){
            return $noti = ['type'=>'error','message'=>'Đang có '.$count.' sản phẩm!'];
         }else{
            $color->delete();
            return $noti = ['type'=>'success','message'=>'Deleted successfully!'];
         }
    }
}
