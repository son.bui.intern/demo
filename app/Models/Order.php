<?php

namespace App\Models;
use App\Models\Order_detail;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable= ['client_id','status','total_amount','name','address','phone','payment_id','shipping_id'];

    public function payment()
    {
        return $this->hasOne(Payment::class,'id','payment_id');
    }
    public function shipping()
    {
        return $this->hasOne(Shipping::class,'id','shipping_id');
    }
    public function user_order()
    {
        return $this->hasOne(Client::class,'id','client_id');
    }
    public function order_detail()
    {
        return $this->hasMany(Order_detail::class,'order_id','id');
    }
    public function scopeSearch($query){
        if(request()->key){
            $keyword = request()->key;
            $query->where('name','LIKE','%'.$keyword.'%');
        }
        return $query;
    }
    public function scopeRemove($query,$id){
        $order = $this->find($id);
        Order_detail::where('order_id',$order->id)->delete();
        $model = $order->delete();
        if($model){
            return $noti = ['type'=>'success','message'=>'Deleted successfully!'];
        }
        return $noti = ['type'=>'error','message'=>'Delete failed!'];
    }
}
