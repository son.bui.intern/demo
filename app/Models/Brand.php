<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $fillable = ['name', 'slug','image'];

    public function product(){
        return $this->hasMany(Product::class,'brand_id','id');
    }

    public function scopeSearch($query){
        if(request()->key){
            $keyword = request()->key;
            $query->where('name','LIKE','%'.$keyword.'%');
        }
        return $query;
    }
    public function scopeAdd(){
        $model = $this->create([
            'name'=>request()->name,
            'slug'=>\Str::slug(request()->name),
            'image'=>request()->image,
        ]);
        if($model){
            return $noti = ['type'=>'success','message'=>'Successfully added!'];
        }else{
            return $noti = ['type'=>'error','message'=>'New addition failed!'];
        }
    }
    public function scopeModify($query,$id){
        $brand =$this->find($id);
        if(request()->image){
            request()->merge(['image'=>request()->image]);
        }
        request()->merge(['slug'=>\Str::slug(request()->name),]);
        $model = $brand->update(request()->except('_token','_method','id'));
        if($model){
            return $noti = ['type'=>'success','message'=>'Successfully fixed!'];
        }else{
            return $noti = ['type'=>'error','message'=>'Fix failed!'];
        }
    }
    public function scopeRemove($query,$id){
        $pro = $this->find($id);
        $count = $pro->product()->count();
        if($count>0){
            return $noti = ['type'=>'error','message'=>'Đang có '.$count.' sản phẩm!'];
        }else{
            $pro->delete();
            return $noti = ['type'=>'success','message'=>'Deleted successfully!'];
        }
    }
}
