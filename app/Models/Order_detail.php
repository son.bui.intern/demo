<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order_detail extends Model
{
    protected $fillable = ['order_id','product_detail_id','quantity','price'];

    public function getproductId()
    {
        return $this->hasOne(Product_detail::class,'id','product_detail_id');
    }
}
