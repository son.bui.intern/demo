<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ConfigWeb extends Model
{
    public $fillable = ['logo_top','logo_bot','email','hotline','address_contact'];

    public function scopeAdd(){
        $model = $this->create([
            'logo_top'=>request()->logo_top,
            'logo_bot'=>request()->logo_bot,
            'email'=>request()->email,
            'hotline'=>request()->hotline,
            'address_contact'=>request()->address_contact,
        ]);
        if($model){
            return $noti = ['type'=>'success','message'=>'Successfully added!'];
        }else{
            return $noti = ['type'=>'error','message'=>'New addition failed!'];
        }
    }
    public function scopeModify($query,$id){
        $config = $this->find($id);
        if(request()->logo_top || request()->logo_bot){
            if(request()->logo_top==request()->old_logo_top){
                request()->merge(['logo_top'=>request()->old_logo_top]);
            }else{
                request()->merge(['logo_top'=>request()->logo_top]);
            }
            if(request()->logo_bot==request()->old_logo_bot){
                request()->merge(['logo_bot'=>request()->old_logo_bot]);
            }else{
                request()->merge(['logo_bot'=>request()->logo_bot]);
            }
        }
        $model = $config->update(request()->except('_token','_method','id'));
        if($model){
            return $noti = ['type'=>'success','message'=>'Successfully fixed!'];
        }else{
            return $noti = ['type'=>'error','message'=>'Fix failed!'];
        }
    }
}