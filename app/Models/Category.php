<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
   protected $fillable = ['name', 'slug'];

   public function product(){
      return $this->hasMany(Product::class,'category_id','id');
   }

   public function getblog(){
      return $this->hasMany(Blog::class,'category_id','id');
   }

   public function scopeSearch($query){
      if(request()->key){
          $keyword = request()->key;
          $query->where('name','LIKE','%'.$keyword.'%');
      }
      return $query;
   }
   public function scopeAdd(){
      $model = $this->create([
         'name'=>request()->name,
         'slug'=>\Str::slug(request()->name),
      ]);
      if($model){
         return $noti = ['type'=>'success','message'=>'Successfully added!'];
      }else{
         return $noti = ['type'=>'error','message'=>'New addition failed!'];
      }
   }
   public function scopeModify($query,$id){
      $model = $this->where('id',$id)->update([
         'name'=>request()->name,
         'slug'=>\Str::slug(request()->name),
      ]);
      if($model){
         return $noti = ['type'=>'success','message'=>'Successfully fixed!'];
      }else{
         return $noti = ['type'=>'error','message'=>'Fix failed!'];
      }
   }
   public function scopeRemove($query,$id){
      $pro =$this->find($id);
      $count = $pro->product()->count();
      if($count > 0){
         return $noti = ['type'=>'error','message'=>'Đang có '.$count.' sản phẩm!'];
      }else{
         $pro->delete();
         return $noti = ['type'=>'success','message'=>'Deleted successfully!'];
      }
   }
}