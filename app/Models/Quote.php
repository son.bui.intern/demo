<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Quote extends Model
{
   protected $fillable = ['name','address','message'];

   public function scopeSearch($query){
      if(request()->key){
         $keyword = request()->key;
         $query->where('name','LIKE','%'.$keyword.'%');
      }
      return $query;
   }

   public function scopeAdd(){
      $model = $this->create([
         'name'=>request()->name,
         'address'=>request()->address,
         'message'=>request()->message,
      ]);
      if($model){
         return $noti = ['type'=>'success','message'=>'Successfully added!'];
      }else{
         return $noti = ['type'=>'error','message'=>'New addition failed!'];
      }
   }

   public function scopeModify($query,$id){
      $model = $this->where('id',$id)->update([
         'name'=>request()->name,
         'address'=>request()->address,
         'message'=>request()->message
      ]);
      if($model){
         return $noti = ['type'=>'success','message'=>'Successfully fixed!'];
      }else{
         return $noti = ['type'=>'error','message'=>'Fix failed!'];
      }
   }
   public function scopeRemove($query,$id){
      $quote =$this->find($id);
      $model = $quote->delete();
      if($model){
          return $noti = ['type'=>'success','message'=>'Deleted successfully!'];
      }else{
          return $noti = ['type'=>'error','message'=>'Delete failed!'];
      }
  }
}