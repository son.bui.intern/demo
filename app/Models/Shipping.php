<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Shipping extends Model
{
    protected $fillable = ['name','status','priority'];

    public function scopeSearch($query){
        if(request()->key){
            $keyword = request()->key;
            $query->where('name','LIKE','%'.$keyword.'%');
        }
        return $query;
    }
    public function scopeAdd(){
        $model = $this->create([
           'name'=>request()->name,
           'status'=>request()->status,
           'priority'=>request()->priority,
        ]);
        if($model){
           return $noti = ['type'=>'success','message'=>'Successfully added!'];
        }else{
           return $noti = ['type'=>'error','message'=>'New addition failed!'];
        }
     }
    public function scopeModify($query,$id){
        $shipping = $this->find($id);
        $model = $shipping->update(request()->except('id','_token','_method'));
        if($model){
            return $noti = ['type'=>'success','message'=>'Successfully fixed!'];
        }else{
            return $noti = ['type'=>'error','message'=>'Fix failed!'];
        }
    }
    public function scopeRemove($query,$id){
        $shipping =$this->find($id);
        $model = $shipping->delete();
        if($model){
            return $noti = ['type'=>'success','message'=>'Deleted successfully!'];
        }else{
            return $noti = ['type'=>'error','message'=>'Delete failed!'];
        }
    }
}
