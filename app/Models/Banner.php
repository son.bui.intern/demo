<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helper\Uploads;

class Banner extends Model
{
    protected $fillable = ['name', 'description','image','status','link','priority','position'];
    // public function product(){
    //     return $this->hasMany(Product::class,'banner_id','id');
    // }

    public function scopeSearch($query){
        if(request()->key){
            $keyword = request()->key;
            $query->where('name','LIKE','%'.$keyword.'%');
        }
        return $query;
    }
    public function scopeAdd(){
        $model = $this->create([
            'name'=>request()->name,
            'image'=>request()->image,
            'description'=>request()->description,
            'status'=>request()->status,
            'link'=>request()->link,
            'priority'=>request()->priority,
            'position'=>request()->position,
        ]);
        if($model){
            return $noti = ['type'=>'success','message'=>'Successfully added!'];
        }else{
            return $noti = ['type'=>'error','message'=>'New addition failed!'];
        }
    }
    public function scopeModify($query,$id){
        $banner =$this->find($id);
        request()->merge(['image'=>request()->image]);
        $model = $banner->update(request()->except('_token','_method','id'));
        if($model){
            return $noti = ['type'=>'success','message'=>'Successfully fixed!'];
        }else{
            return $noti = ['type'=>'error','message'=>'Fix failed!'];
        }
    }
    public function scopeRemove($query,$id){
        $banner =$this->find($id);
        $model = $banner->delete();
        if($model){
            return $noti = ['type'=>'success','message'=>'Deleted successfully!'];
        }else{
            return $noti = ['type'=>'error','message'=>'Delete failed!'];
        }
    }
}
