<?php

namespace App\Http\Requests\user;

use Illuminate\Foundation\Http\FormRequest;

class UserEditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        if(request()->password){
            return [
                'name' => 'required',
                'email' => 'required|email|unique:users,email,'.request()->id,
                'phone' => 'unique:users,phone,'.request()->id,
                'confirm_password' => 'required|same:password',
            ];
        }
        return [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.request()->id,
            'phone' => 'unique:users,phone,'.request()->id,
        ];
    }

    public function messages()
    {
        return [
            //
        ];
    }
}
