<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ConfigWeb;

class ConfigWebController extends Controller
{
    public function index()
    {
        $config = ConfigWeb::first();
        return view('admin.configWeb.index',compact('config'));
    }
    public function create()
    {
        return view('admin.configWeb.add');
    }
    public function store()
    {
        $ms = ConfigWeb::add();
        return redirect()->route('admin.config.index')->with($ms['type'],$ms['message']);
    }
    public function edit($id)
    {
        $config = ConfigWeb::find($id);
        return view('admin.configWeb.edit',compact('config'));
    }
    public function update($id)
    {
        $ms = ConfigWeb::modify($id);
        return redirect()->route('admin.config.index')->with($ms['type'],$ms['message']);
    }
}
