<?php

namespace App\Http\Controllers\admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Auth;
use App\Http\Requests\admin\AdminLoginRequest;
use App\Http\Requests\admin\AdminRegisterRequest;
use App\Helper\CartHelper;
use App\Models\Client;
use App\Models\Order;
use App\Models\Rating;
use App\Http\Requests\user\ChangePasswordRequest;

class AdminController extends Controller
{
    public function index()
    {
        $client = Client::all();
        $order = Order::all();
        $rating = Rating::all();
        return view('admin.index',compact('client','order','rating'));
    }

    public function login()
    {
        return view('admin.login');
    }

    public function post_login(AdminLoginRequest $request)
    {
        $info = request()->only('email','password');
        if(Auth::attempt($info)){
            return redirect()->route('admin.index')->with('success','Logged in successfully!');
        }else{
            return redirect()->back()->with('error','Login failed!');
        }
    }

    public function register()
    {
        return view('admin.register');
    }

    public function post_register(AdminRegisterRequest $request)
    {
        User::add();
        return redirect()->route('admin.login')->with('success','Sign up success!');
    }

    public function logout(){
        Auth::logout();
        return redirect()->route('admin.login')->with('success','Logged out successfully!');
    }

    public function error(){
        $code = request()->code;
        $errors = config('error.'.$code);
        return view('admin.error',$errors);
    }
    public function profile(){
        return view('admin.profile');
    }
    public function form_password(){
        return view('admin.changepassword');
    }
    public function post_profile($id){
        User::change_profile($id);
        return redirect()->route('admin.index')->with('success','Change profile!');
    }
    public function post_avatar($id){
        User::change_avatar($id);
        return redirect()->route('admin.index')->with('success','Change Avatar!');
    }
    public function post_password(ChangePasswordRequest $request,$id){
        $ms = User::change_password($id);
        return redirect()->route($ms['route'])->with($ms['type'],$ms['message']);
    }
}
